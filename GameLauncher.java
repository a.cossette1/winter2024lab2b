import java.util.Scanner;

public class GameLauncher {
	public static void main(String[] args) {
		boolean willToPlay = true;
		
		while(willToPlay) {
			Scanner reader = new Scanner(System.in);
			System.out.println("Hello user please Select which Game you would like to play:");
			System.out.println("Press '1' for Hangman");
			System.out.println("Press '2' for Wordle");
			
			int answer = reader.nextInt();
			
			if(answer == 1){
				System.out.println("Enter the word that needs to be guessed: ");
				String word = reader.next();
				Hangman.clearTerminal();

				if(word.length() == 4 && word.chars().distinct().count() == 4){
					Hangman.runGame(word);
				}
				else{
					System.out.println("The word must be a 4 Letter word with all Unique letters!");
				}
			}
			else{
				String wordToGuess = Wordle.generateWord();
				Wordle.runGame(wordToGuess);
			}
			
			System.out.println("Would you like to play again? Type: 'Yes' or 'No'");
			String wantPlay = reader.next();
			
			if(wantPlay.equals("No")){
				willToPlay = false;
			}
		}
	}
}