import java.util.Scanner;

public class Hangman{
	public static void main(String[] args){
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the word that needs to be guessed: ");
		String word = scanner.nextLine();
		clearTerminal();

		if(word.length() == 4 && word.chars().distinct().count() == 4){
			runGame(word);
		}
		else{
			System.out.println("The word must be a 4 Letter word with all Unique letters!");
		}
	}
	public static int isLetterInWord(String word, char c){
		for(int i = 0; i < word.length(); i++){
			if(toUpperCase(word.charAt(i)) == toUpperCase(c)){
				return i;
			}
		}
		return -1;
	}
	public static char toUpperCase(char c){
		return Character.toUpperCase(c);
	}
	public static void printWork(String word, boolean[] letters){
		String display = "";

		for(int i = 0; i < letters.length; i++){
			if(letters[i] == true){
				display += word.charAt(i);
			}
			else{
				display += "_ ";
			}
		}
		System.out.println(display);
	}
	public static void runGame(String word){
		Scanner reader = new Scanner(System.in);
		boolean[] chars = new boolean[]{false, false, false, false};
		int i = 0;

		while(i<6){
			System.out.println("Enter a letter to be guessed");
			char guess = reader.nextLine().charAt(0);
			int guessCheck = isLetterInWord(word, guess);

			if(guessCheck == -1){
				i++;
				System.out.println("Wrong! Try Again! L");
			}
			else{
				chars[guessCheck] = true;
				System.out.println("Good Job you got it right!");
			}
			printWork(word, chars);
			if(chars[0] == true && chars[1] == true && chars[2] == true && chars[3] == true){
				i = 7;
			}
		}
		if(chars[0] == true && chars[1] == true && chars[2] == true && chars[3] == true){
			System.out.println("Congratulations! You win!");
		}
	}
	public static void clearTerminal(){
		System.out.print("\033[H\033[2J");
		System.out.flush();
	}
}