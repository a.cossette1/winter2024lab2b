import java.util.Random;
import java.util.Scanner;

public class Wordle {
    /*public static void main(String[] args){
        String wordToGuess = generateWord();
        runGame(wordToGuess);
    }
	*/
    public static String generateWord() {
        String[] words = new String[] {"ALONE", "BRAIN", "COURT", "DOUBT", "EARLY", "FRUIT", "GRANT", "HEART", "IDEAL", "JUICE", "KNIFE", "LEAST", "MOUSE", "NOISE", "OCEAN", "PRINT", "QUIET", "ROUTE", "STORE", "TRAIL"};
        Random random = new Random();
        return words[random.nextInt(words.length)];
    }

    public static boolean letterInWord(String word, char letter) {
        for(int i = 0; i < word.length(); i++) {
            if (word.charAt(i) == letter) {
                return true;
            }
        }
        return false;
    }

    public static boolean letterInSlot(String word, char letter, int position){
        if (position < 0 || position >= word.length()) {
            return false;
        }
    
        for (int i = 0; i < word.length(); i++) {
            if (i == position && word.charAt(i) == letter) {
                return true;
            }
        }
        return false;
    }

    public static String[] guessWord(String answer, String guess) {
        String[] newArray = new String[5];

        for(int i = 0; i < newArray.length; i++){
            if(guess.charAt(i) == answer.charAt(i)){
                newArray[i] = "green";
            }
            else if(guess.charAt(i) != answer.charAt(i) && guess.charAt(i) == answer.charAt(0) || guess.charAt(i) == answer.charAt(1) || guess.charAt(i) == answer.charAt(2) || guess.charAt(i) == answer.charAt(3) || guess.charAt(i) == answer.charAt(4)){
                newArray[i] = "yellow";
            }
            else{
                newArray[i] = "white";
            }
        }
        return newArray;
    }
    
    public static void presentResults(String word, String[] colours){
        for (int i = 0; i < 5; i++) {
            if ("green".equals(colours[i])) {
                System.out.print("\u001B[32m" + word.charAt(i) + "\u001B[0m");
            } else if ("yellow".equals(colours[i])) {
                System.out.print("\u001B[33m" + word.charAt(i) + "\u001B[0m");
            } else if ("white".equals(colours[i])) {
                System.out.print(word.charAt(i));
            }
        }
        System.out.println();
    }
    
    public static String readGuess() {
        Scanner scanner = new Scanner(System.in);
        String guess;
        
        do {
            System.out.print("Enter the Word You would Like to Guess: ");
            guess = scanner.nextLine().toUpperCase();
        } while(guess.length() != 5);
        return guess;
    }

    public static void runGame(String word){
        int attempts = 0;
        while (attempts < 6) {
            String guess = readGuess();
            String[] colors = guessWord(word, guess);
            presentResults(guess, colors);

            if (guess.equals(word)) {
                System.out.println("Good job! You Win!");
                return;
            } else {
                System.out.println("Try again. Attempts left: " + (5 - attempts));
            }
            attempts++;
        }
        System.out.println("Sorry, you didn't guess the word. The correct word was: " + word);
    }
}